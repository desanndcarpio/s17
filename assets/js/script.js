//Create array

//Array Literal []

//const array1 = ['eat', 'sleep'];
//console.log(array1);

//using new Array

//const array2 = new Array('eat', 'play');
//console.log (array2);

/*
	Types of Array
*/

//empty array

// const myList = []; 


// //Number Array
// const numArray = [2,3,4,5];

// //String Array
// const stringArray = ['a', 'b','c'];


// //Object, Function, array  //mixed data
// const newData1 = [
// 		{'task1':'exercise'},
// 		[1,2,3],
// 		function hello(){
// 			console.log('Hi I am array')
// 		}
// 	];
// console.log(newData1);

// /*
// 	Mini Activity

// */

let placesToVisit = [
	'Japan',
	'Korea',
	'Hongkong', 
	'SG', 
	'US', 
	'Thailand', 
	'Europe'
];

// //first element
// console.log(placesToVisit[0]);

// //display the array
// console.log(placesToVisit);

// //display the last element
// console.log(placesToVisit[placesToVisit.length-1]);

// //display each element of the array
// for (let i = 0; i <= placesToVisit.length - 1; i++){
// 	console.log(placesToVisit[i]);
// }


/*********************/

// Array Manipulation

//push()

let dailyActivities = ['eat', 'work','pray', 'play'];
dailyActivities.push('exercise');
console.log(dailyActivities);

//unshift() beginning

dailyActivities.unshift('sleep');
console.log(dailyActivities);

// replace element/ reassign

dailyActivities[2]= 'sing'
console.log(dailyActivities);

//adding at the end of the array
dailyActivities[6] = 'dance'; //adding at the end you must know the last index
console.log(dailyActivities);

//Re assign values in an array

placesToVisit[3] = "Giza Sphinx";
console.log(placesToVisit); 

console.log(placesToVisit[5]);
placesToVisit[5] = "Turkey";
console.log(placesToVisit[5]);

/*
	Mini Activity
		Re assigning values first and last value
*/

placesToVisit[0] = 'Antipolo';

//console.log(placesToVisit.length-1);

placesToVisit[placesToVisit.length-1] = 'SBS QC';

console.log(placesToVisit[0]);
console.log(placesToVisit[placesToVisit.length-1]);

//adding items without using methods

let array= [];
console.log(array[0]);
array[0] = 'Cloud Strife';
console.log(array);
console.log(array[1]);
array[1] = 'Tifa Lockhart';
console.log(array[1]);

//reassign the last element
array[array.length-1] = 'Aerith Gainsborough';
console.log(array);

//add element at the end
array[array.length] = 'Vincent Valentine';
console.log(array);

/*
	Other Array Methods
*/

//Manipulate arrays with predetermined JS function
// Mutators

	let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];
	// without method
	array1[array.length] = 'Francisco';
	console.log(array1);

	//.push() 

	array1.push('Andres');
	console.log(array1);

	//unshift

	array1.unshift('Simon');
	console.log(array1);

	// .pop()

	array1.pop();
	console.log(array1);

	// .pop() also able to returen the item removed

	console.log(array1.pop());
	console.log(array1);

	let removedItem = array1.pop();
	console.log(array1);
	console.log(removedItem);

	// .shift()

	let removedItemShift = array1.shift();
	console.log(array1); 
	console.log(removedItemShift);

/*
	Mini-Activity
		Array Methods

*/
//remove first item of array 1 
	array1.shift();
	console.log(array1); 

//remove last item of array 1
	array1.pop();
	console.log(array1);

//add george at the start of array1
	array1.unshift('George');
	console.log(array1);

//add michael at the end of array1
	array1.push('Michael');
	console.log(array1);



// .sort()

array1.sort();
console.log(array1);

let numArray = [3,2,1,6,7,9];
numArray.sort();
console.log(numArray);


let numArray2 = [32, 400, 450, 2, 9, 5, 50, 90];
numArray2.sort();
console.log(numArray2);

//to sort descending numerically (anonymous function)
let numArray3 = [32, 400, 450, 2, 9, 5, 50, 90];
numArray3.sort((a,b)=> b-a);
console.log(numArray3);


//to sort ascending numerically (anonymous function)
let numArray4 = [32, 400, 450, 2, 9, 5, 50, 90];
numArray4.sort((a,b)=> a-b);
console.log(numArray4);



//ascending sort per number's value 
/*
	same as

	numArray4.sort((a,b)=> a-b);
	console.log(numArray4);
*/
numArray2.sort(function(a,b){
	return a-b;
});
console.log(numArray2);


//string sort()

let arrayStr = [ 
	'Marie',
	'Zen',
	'Jamie',
	'Elain'
];

// arrayStr.sort(function(a,b){
// 	return b-a
// });
// console.log(arrayStr);

arrayStr.sort()
console.log(arrayStr)


//reverse()

arrayStr.sort().reverse();
console.log(arrayStr);

// splice()

let beatles = ['George', 'John', 'Paul', 'Ringo' ];
let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'Kobe', 'Shaq'];

lakersPlayers.splice(0, 0, 'Caruso'); // added at index 0, no elements deleted, 'caruso' added
console.log(lakersPlayers);

lakersPlayers.splice(0,1); //caruso removed no added item
console.log(lakersPlayers);

lakersPlayers.splice(0,3); //lebron, davis, westbrook removed 3 items removed
console.log(lakersPlayers); 

lakersPlayers.splice(1,1); 
console.log(lakersPlayers); //shaq removed 

lakersPlayers.splice(1,0, 'Gasol', 'Fisher'); //added from index 1 0 deleted
console.log(lakersPlayers);


/*
	Non Mutators
	
*/

//slice()

let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];

computerBrands.splice(2,2,'Compaq', 'Toshiba', 'Acer'); // starting from index[2] removed 2 items and added 3 items
console.log(computerBrands);

let newBrands = computerBrands.slice(1,3); //added new array from computerBrands[1] and BEFORE the computerBrands[3]. [3] is not included
console.log(newBrands);

let fonts = ['TNR', 'Comic Sans MS', 'Impact', 'Mono Cors', 'Arial', 'Arial Black'];
console.log(fonts);

let newFontSet = fonts.slice(1,4); //[1],[2],[3]
console.log(newFontSet);

newFontSet = fonts.slice(2); //added in newFontSet from index [2] to end of index
console.log(newFontSet);

newFontSet = fonts.slice(); //no index specified will return everything

newFontSet = fonts.slice().reverse(); // will reverse 
console.log(newFontSet);

/*
	Mini-Activity 
		Splice & Slice
*/


let videoGame = [
	'PS4',
	'PS5',
	'Switch',
	'Xbox',
	'Xbox1'
]; 

//slice the last two items in the array
let microsoft = videoGame.slice(3);

//slice the third and fourth items in the array
let nintendo = videoGame.slice(2,4);

//console
console.log(microsoft);
console.log(nintendo);

/***********************/

// .toString()

let sentence = ['I', 'like', 'JavaScript', '.', 'It', 'is', 'fun'];
let sentenceString = sentence.toString();
console.log(sentence);
console.log(sentenceString);

// .join()

let sentence2 = ['My', 'favorite', 'fastfood', 'is', 'Army Navy'];
let sentenceString2 = sentence2.join(" ");
console.log(sentence2);
console.log(sentenceString2);


let sentenceString3 = sentence2.join("");
console.log(sentenceString3);

/*
	Mini-Activity

/* 			
			Mini-Activity

			Given a set of characters, 
				-form the name, "Martin" as a single string.
				-form the name, "Miguel" as a single string.
			Use the methods we discussed so far.

			save "Martin" and "Miguel" to variables:
				-name 1 and name 2

			log both variables on the console.

*/

let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];

//form the name, "Martin" as a single string.
let name1 = charArr.slice(5,11).join('');
//name1 = name1.join('');
console.log(name1);

//form the name, "Miguel" as a single string.
let name2 = charArr.slice(13,19).join('');
//name2 = name2.join('');
console.log(name2);

/**************/

// .concat()

let taskFriday = ['drink HTML', 'eat JavaScript'];

let taskSaturday = ['inhale CSS', 'breath Bootstrap'];

let taskSunday = ['get Git', 'be Node'];

let weekendTask = taskFriday.concat(taskSaturday, taskSunday); //will combine sat and sun in friday

console.log(weekendTask);

/**********************************************/

/*
	Accessors

*/

// indexOf()
	
let batch131 = [ 
		'Paolo',
		'Jamir',
		'Jed',
		'Ronel',
		'Rom',
		'Jayson'
];

//index value of Jed
console.log(batch131.indexOf("Jed")); //2

// lastIndexOf()
console.log(batch131.lastIndexOf('Jamir')); //1


/*
	Mini Activity

	Given a set of brands with some entries repeated:
		Create a function which can display the index of the brand that was input the first time it was found in the array.

		Create a function which can display the index of the brand that was input the last time it was found in the array.

*/

let carBrands = [
		'BMW',
		'Dodge',
		'Maserati',
		'Porsche',
		'Chevy',
		'Ferrari',
		'GMC',
		'Porsche',
		'Mitsubishi',
		'Toyota',
		'Volks',
		'BMW'
];

//Create a function which can display the index of the brand that was input the first time it was found in the array.
function displayIndexFirst(brand){
	console.log(carBrands.indexOf(brand));
}

//Create a function which can display the index of the brand that was input the last time it was found in the array.
function displayIndexLast(brand){
	console.log(carBrands.lastIndexOf(brand));
}

displayIndexFirst('BMW'); //0
displayIndexLast('BMW'); //11

/*******************/

/*
	Iterator Methods

*/

// forEach()

let avengers = [ 
		'Hulk',
		'Black Widow',
		'Hawkeye',
		'Spiderman',
		'IronMan',
		'Captain America'

];
				//anonymous function
avengers.forEach(function(avenger){
	console.log(avenger);
})


let marvelHeroes = [ 
		'Moon Knight',
		'Jessica Jones',
		'Deadpool',
		'Cyclops'
];

console.log(avengers);


//iterate all the items in Marvel heroes array and let them join the avengers
marvelHeroes.forEach(function(hero){
	//cyclops and deadpool is not allowed to join
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		avengers.push(hero);
	}
});

console.log(avengers);


// map()

let number = [25, 50, 30, 10, 5];

let mappedNumbers = number.map(function(number){
	console.log(number);
	return number * 5
});

console.log(mappedNumbers);

// every() 

let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >= 18;
})

console.log(checkAllAdult); //false 


// some()

let examScores = [75, 80, 74, 71];

let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >= 80;
});

console.log(checkForPassing);

// filter()

let numbersArr2 = [500, 12, 120, 60, 6, 30];

let divisibleBy5 = numbersArr2.filter(function(number){
	return number % 5 === 0;
});

console.log(divisibleBy5);


// find() - login validate match

let registerdUsernames = ['pedro101', 'mickeyTheKing2000', 'superPheonix', 'sheWhoCode'];

let foundUser = registerdUsernames.find(function(username){
	console.log(username);
	return username === 'mickeyTheKing2000';
});

console.log(foundUser);

// includes()

let registeredEmails = [
		'johnnyPheonix1991@gmail.com',
		'michaelKing@gmail.com',
		'pedro_himself@yahoo.com',
		'sheJonesSmith@gmail.com'
];

let doesEmailExist = registeredEmails.includes('michaelKing@gmail.com');
console.log(doesEmailExist); //true

console.log('***************');

/*
	Mini Activity

		Create 2 functions 
		First Function is able to find specified or the username input in our registeredUsernames array.
		Display the result in the console.

		Second Function is able to find a specified email already exist in the registeredEmails array.
			- IF  there is an email found, show an alert:
				"Email Already Exist."
			- IF there is no email found, show an alert:
				"Email is available, proceed to registration."

		You may use any of the three methods we discussed recently.

*/
//First Function is able to find specified or the username input in our registeredUsernames array. Display the result in the console.


function findUsername(username){
	registerdUsernames.find(function(username){
		console.log(username);
		return username === 'sheWhoCode';
	});
};

findUsername('sheWhoCode');

/*

Second Function is able to find a specified email already exist in the registeredEmails array.
			- IF  there is an email found, show an alert:
				"Email Already Exist."
			- IF there is no email found, show an alert:
				"Email is available, proceed to registration."

*/


function findValidEmail(email){
	if (registeredEmails.includes(email)){
		alert('Email does exist.')
	} else {
		alert('Email is available, proceed to registration');
	}
};

findValidEmail('sheJonesSmith@yahoo.com');



